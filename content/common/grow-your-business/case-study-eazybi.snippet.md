<h2 class="title">eazyBI</h2>

Ater 15 years of working with large vendors to implement business intelligence products, Raimonds Simanovskis, founder and CEO of eazyBI, saw a need for a user-friendly business intelligence tool for the average user. After finding the Atlassian Ecosystem and building an app for Jira, the Atlassian Marketplace has since become eazyBI's most successful sales channel. 

<a href="/showcase/eazyBI/">Learn more<i class="fa fa-arrow-right" aria-hidden="true"></i></a>