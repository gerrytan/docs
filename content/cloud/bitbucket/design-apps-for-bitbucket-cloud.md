---
title: "Designing apps for Bitbucket Cloud"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: learning
learning: examples 
date: "2015-09-17"
---

# Designing apps for Bitbucket Cloud

We've compiled some best practice guidance to help you design apps which are visually appealing within the Bitbucket Cloud UI. There are several factors to consider when designing both the visual look and the workflow: 

* Users will likely have more than one app, reduce UI clutter by not adding several items in the same extension point where possible. 
* Users might not recognize what is your app and what is Bitbucket. Identify your app with a unique title or other identifying factor. 
* Drastic changes in experience can be jarring for users. While users should be able to identify your app you can make it more consistent with the Bitbucket and Atlassian experience by following the [Atlassian Design Guidelines](https://design.atlassian.com) (ADG).

## Using the ADG and Atlaskit
The ADG is a set of guidelines that Atlassian uses when designing its products, including Bitbucket.
In order for the user to have a consistent experience across Bitbucket, we strongly recommend that follow the ADG when designing your app.

To easily follow the ADG, you can use [Atlaskit](https://atlaskit.atlassian.com/). Atlaskit is Atlassian's official UI library,
which we use to build our cloud products. It is a collection of [React](https://facebook.github.io/react/) components that
you can use within your app. If you would prefer not to use React, the [reduced UI pack](http://aui-cdn.atlassian.com/atlaskit/registry/reduced-ui-pack/latest/index.html)
contains css classes for styling common html elements.

## Designing for Bitbucket Cloud extension points

### Repository sidebar

The extension points in the repository sidebar are used to link to pages or processes inside or outside of Bitbucket Cloud. The actions and the navigation sections of the sidebar both contain extension points.

### Repository overview panel

The extension points in the repository overview section are 'webPanel' items which appear below the repository details section and above the README section. You can enhance or extend the experience of the repository overview by displaying relevant content in these extension points.

Repository overview webPanels will have:

   * a border defined by Bitbucket: 1px border #cccccc (ash gray)
   * padding specified by the developer (suggested 20px)
   * a border-radius of 3px
   * a top and bottom margin of 5px on all main column apps
   * a 10px bottom margin below repo details
   * an extra 10px bottom below last app
   * a max-height: 280px

Best practice:

  * If the app has a lot of whitespace consider centering content
  * We suggest following [ADG](https://design.atlassian.com) and using [Atlaskit](https://atlaskit.atlassian.com) for better overall experience and consistency
  * If the app needs more space, consider making a full page in order to link to
  * Apps look best designed at 538/270px and made responsive
  * Use as a brief overview of actual app, link to a full-page app
  * Have 10px padding
  * Have content fit within 280px to avoid scrollbars

### Repository page

The repository page extension point allows you to use a full page within Bitbucket Cloud for your app. 

Repository pages will: 

* still show the Bitbucket header, footer, and sidebar. 
* not have a border or padding enforced by Bitbucket.
* have a height based on the content within the iframe.

Best practice:

* Follow the [Atlassian Design Guidelines](https://design.atlassian.com) for a better overall experience and consistency with the Bitbucket experience.
* Add an h1 title to create a more integrated experience. 
* Use `20px` padding for the page. 
* Use a single sidebar link to a main page if you have multiple pages and link them from. 
