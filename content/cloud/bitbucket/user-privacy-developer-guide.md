---
title: "User Privacy Developer Guide"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: other
date: "2019-03-22"
---

{{< include path="docs/content/cloud/connect/guides/user-privacy-developer-guide.snippet.md" >}}
{{< include path="docs/content/cloud/connect/guides/personal-data-reporting-api-bb.snippet.md" >}}