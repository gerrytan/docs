---
title: "Understanding JWT for apps"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: intro 
date: "2016-09-06"
---

# Understanding JWT for apps

There is a [nicely presented copy](http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html) of the specification.
JSON Web Token (JWT) is a compact URL-safe means of representing claims to be transferred between two parties.
The claims in a JWT are encoded as a JavaScript Object Notation (JSON) object that is used as the payload of a
JSON Web Signature (JWS) structure or as the plaintext of a JSON Web Encryption (JWE) structure, enabling the claims to
be digitally signed or MACed and/or encrypted.

## Structure of a JWT token

A JWT token looks like this:

``` html
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjEzODY4OTkxMzEsImlzcyI6ImppcmE6MTU0ODk1OTUiLCJxc2giOiI4MDYzZmY0Y2ExZTQxZGY3YmM5MGM4YWI2ZDBmNjIwN2Q0OTFjZjZkYWQ3YzY2ZWE3OTdiNDYxNGI3MTkyMmU5IiwiaWF0IjoxMzg2ODk4OTUxfQ.uKqU9dTB6gKwG6jQCuXYAiMNdfNRw98Hw_IWuA5MaMo
```

Once you understand the format, it's actually pretty simple:

``` html
<base64-encoded header>.<base64-encoded claims>.<base64-encoded signature>
```

In other words:

* You create a header object, with the JSON format, then you encode it as a base64.
* You create a claims object, with the JSON format, then you encode it in base64.
* You create a signature for the URI (we'll get into that later), then you encode it in base64.
* You concatenate the three items, with the "." separator.

You shouldn't actually have to do this manually, as there are libraries available in most languages, as we describe in the [JWT libraries](#jwt-libraries) section.
However it is important you understand the fields in the JSON header and claims objects described in the next sections.

### JWT header  

The JWT header declares that the encoded object is a JSON Web Token (JWT) and the JWT is a JWS that is MACed using the HMAC SHA-256 algorithm. For example:

``` json
{
    "typ":"JWT",
    "alg":"HS256"
}
```

<table>
    <thead>
        <tr>
            <th>Attribute</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tr>
        <td><code>typ</code> (mandatory)</td><td>String</td><td>Type for the token, defaulted to <code>JWT</code>; specifies that this is a JWT token.</td>
    </tr>
    <tr>
        <td><code>alg</code> (mandatory)</td><td>String</td><td>Algorithm; specifies the algorithm used to sign the token.
            In atlassian-connect version 1.0 we support the HMAC SHA-256 algorithm, which the
            <a href="http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-13">JWT specification</a> identifies using the string <a href="http://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-18#section-3.1">"HS256"</a>.</td>
</table>

{{% note %}}Your JWT library or implementation should discard any tokens which specify `alg: none` as this can provide a bypass of the token verification.{{% /note %}}


### Claims  

The JWT claims object contains security information about the message. For example:

``` javascript
{
    'iss': 'com.example.app',
    'iat': 1433780963,
    'qsh': '88396352255a6c933def07620a3281c9e27d8c668e0f4d01a8ecdbb74ca52c97',
    'sub': 'connection:479',
    'exp': 1437380963
}
```

<table>
    <thead>
        <tr>
            <th>Attribute</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tr>
        <td><code>iss</code> (mandatory)</td>
        <td>String</td>
        <td>The issuer of the claim. Connect uses it to identify the application making the call. For example:
            <ul>
                <li>If the Atlassian product is the calling application: contains the unique identifier of the tenant.
             This is the <code>clientKey</code> that you receive in the <code>installed</code> callback. You should reject unrecognized issuers.
                <li>If the app is the calling application: the app key specified in the app descriptor.
            </ul>
        </td>
    </tr>
    <tr>
        <td><code>iat</code> (mandatory)</td>
        <td>Long</td>
        <td>Issued-at time. Contains the UTC Unix time at which this token was issued. There are no hard
            requirements around this claim but it does not make sense for it to be significantly in the future.
            Also, significantly old issued-at times may indicate the replay of suspiciously old tokens. </td>
    </tr>
    <tr>
        <td><code>exp</code> (mandatory)</td>
        <td>Long</td>
        <td>Expiration time. It contains the UTC Unix time after which you should no longer accept this token.
            It should be after the issued-at time.</td>
    </tr>
    <tr>
        <td><code>qsh</code> (mandatory)</td>
        <td>String</td>
        <td><a href="/cloud/bitbucket/query-string-hash">Query string hash</a>. A custom Atlassian claim that prevents URL tampering.</td>
    </tr>
    <tr>
        <td><code>sub</code> (mandatory)</td>
        <td>String</td>
        <td>The subject of this token. The subject of this token. This is the user associated with the relevant action. In Bitbucket's case the <code>sub</code> claim is associated with the connection key, as shown in the following example. 
{{< highlight javascript >}}{
    'iss': 'com.example.app',
    'iat': 1433780963,
    'qsh': '88396352255a6c933def07620a3281c9e27d8c668e0f4d01a8ecdbb74ca52c97',
    'sub': 'connection:479',
    'exp': 1437380963
}
{{< /highlight >}}
        </td>
    </tr>
    <tr>
        <td><code>aud</code> (Unavailable)</td>
        <td>String or String</td>
        <td>Currently, in Connect for Bitbucket Cloud, you cannot use the <code>aud</code> claim. Doing so will cause your app to fail. </td>
    </tr>
    <tr>
        <td><code>context</code> (optional)</td>
        <td>Object</td>
        <td>The context claim is an extension added by Atlassian Connect which may contain useful context for outbound requests (from the product to your app).

            The current user (the same user in the <code>sub</code> claim) is added to the context. This contains the userKey, username and display name for the subject.

            {{< highlight javascript >}}
"context": {
    "user": {
        "userKey": "batman",
        "username": "bwayne",
        "displayName": "Bruce Wayne"
    }
}
{{< /highlight >}}

            <ul>
                <li><code>userKey</code> - the primary key of the user. Anytime you want to store a reference to a user in long term storage (e.g., a database or index) you should use the key because it can never change. The user key should never be displayed to the user as it may be a non-human readable value.</li>
                <li><code>username</code> - a unique secondary key, but should not be stored in long-term storage because it can change over time. This is the value that the user logs into the application with, and may be displayed to the user.</li>
                <li><code>displayName</code> - the user's name.</li>
            </ul>

        </td>
    </tr>

</table>

You should use a little leeway when processing time-based claims, as clocks may drift apart.
The JWT specification suggests no more than a few minutes.
Judicious use of the time-based claims allows for replays within a limited window.
This can be useful when all or part of a page is refreshed or when it is valid for a user to
repeatedly perform identical actions (e.g., clicking the same button).

## <a id="jwt-libraries"></a>JWT libraries

Most modern languages have JWT libraries available. We recommend you use one of these libraries
(or other JWT-compatible libraries) before trying to hand-craft the JWT token.

<table>
    <thead>
        <tr>
            <th>Language</th>
            <th>Library</th>
        </tr>
    </thead>
    <tr><td>Java</td><td><a href="https://bitbucket.org/atlassian/atlassian-jwt/">atlassian-jwt</a> and <a href="https://code.google.com/p/jsontoken/">jsontoken</a></td></tr>
    <tr><td>Python</td><td><a href="https://github.com/jpadilla/pyjwt">pyjwt</a></td></tr>
    <tr><td>Node.js</td><td><a href="https://github.com/hokaccha/node-jwt-simple">node-jwt-simple</a></td></tr>
    <tr><td>Ruby</td><td><a href="https://github.com/progrium/ruby-jwt">ruby-jwt</a></td></tr>
    <tr><td>PHP</td><td><a href="https://github.com/firebase/php-jwt">firebase php-jwt</a> and <a href="https://github.com/luciferous/jwt">luciferous jwt</a></td></tr>
    <tr><td>.NET</td><td><a href="https://github.com/johnsheehan/jwt">jwt</a></td></tr>
    <tr><td>Haskell</td><td><a href="http://hackage.haskell.org/package/jwt">haskell-jwt</a></td></tr>
</table>

The [JWT decoder](http://jwt-decoder.herokuapp.com/jwt/decode) is a handy web-based decoder for Atlassian Connect JWT tokens.

## Creating a JWT token

Here is an example of creating a JWT token, in Java, using atlassian-jwt and nimbus-jwt:

``` java
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import com.atlassian.jwt.*;
import com.atlassian.jwt.core.writer.*;
import com.atlassian.jwt.httpclient.CanonicalHttpUriRequest;
import com.atlassian.jwt.writer.JwtJsonBuilder;
import com.atlassian.jwt.writer.JwtWriterFactory;

public class JWTSample {

    public String createUriWithJwt()
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        long issuedAt = System.currentTimeMillis() / 1000L;
        long expiresAt = issuedAt + 180L;
        String key = "atlassian-connect-app"; //the key from the app descriptor
        String sharedSecret = "...";    //the sharedsecret key received
                                        //during the app installation handshake
        String method = "GET";
        String baseUrl = "http://localhost:2990/jira";
        String contextPath = "/jira";
        String apiPath = "/rest/api/latest/serverInfo";

        JwtJsonBuilder jwtBuilder = new JsonSmartJwtJsonBuilder()
                .issuedAt(issuedAt)
                .expirationTime(expiresAt)
                .issuer(key);

        CanonicalHttpUriRequest canonical = new CanonicalHttpUriRequest(method,
                apiPath, contextPath, new HashMap());
        JwtClaimsBuilder.appendHttpRequestClaims(jwtBuilder, canonical);

        JwtWriterFactory jwtWriterFactory = new NimbusJwtWriterFactory();
        String jwtbuilt = jwtBuilder.build();
        String jwtToken = jwtWriterFactory.macSigningWriter(SigningAlgorithm.HS256,
                sharedSecret).jsonToJwt(jwtbuilt);

        String apiUrl = baseUrl + apiPath + "?jwt=" + jwtToken;
        return apiUrl;
    }
}
```

## Decoding and verifying a JWT token

Here is a minimal example of decoding and verifying a JWT token, in Java, using atlassian-jwt and nimbus-jwt.
**Note:** This example does not include any error handling.
See [AbstractJwtAuthenticator](https://bitbucket.org/atlassian/atlassian-jwt/src/master/core/src/main/java/com/atlassian/jwt/core/http/auth/AbstractJwtAuthenticator.java)
from atlassian-jwt for recommendations of how to handle the different error cases.

``` java
import com.atlassian.jwt.*;
import com.atlassian.jwt.core.http.JavaxJwtRequestExtractor;
import com.atlassian.jwt.core.reader.*;
import com.atlassian.jwt.exception.*;
import com.atlassian.jwt.reader.*;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

public class JWTVerificationSample {

    public Jwt verifyRequest(HttpServletRequest request,
                             JwtIssuerValidator issuerValidator,
                             JwtIssuerSharedSecretService issuerSharedSecretService)
            throws UnsupportedEncodingException, NoSuchAlgorithmException,
                   JwtVerificationException, JwtIssuerLacksSharedSecretException,
                   JwtUnknownIssuerException, JwtParseException {
        JwtReaderFactory jwtReaderFactory = new NimbusJwtReaderFactory(
                issuerValidator, issuerSharedSecretService);
        JavaxJwtRequestExtractor jwtRequestExtractor = new JavaxJwtRequestExtractor();
        CanonicalHttpRequest canonicalHttpRequest
                = jwtRequestExtractor.getCanonicalHttpRequest(request);
        Map<String, ? extends JwtClaimVerifier> requiredClaims = JwtClaimVerifiersBuilder.build(canonicalHttpRequest);
        String jwt = jwtRequestExtractor.extractJwt(request);
        return jwtReaderFactory.getReader(jwt).readAndVerify(jwt, requiredClaims);
    }
}
```

### Decoding a JWT token

Decoding the JWT token reverses the steps followed during the creation of the token,
to extract the header, claims and signature. Here is an example in Java:

``` java
String jwtToken = ...;//e.g. extracted from the request
String[] base64EncodedSegments = jwtToken.split('.');
String base64EncodedHeader = base64EncodedSegments[0];
String base64EncodedClaims = base64EncodedSegments[1];
String signature = base64EncodedSegments[2];
String header = base64decode(base64EncodedHeader);
String claims = base64decode(base64EncodedClaims);
```

This gives us the following:

Header:

``` javascript
{
    "alg": "HS256",
    "typ": "JWT"
    }
```

Claims:

``` javascript
{
    "iss": "jira:15489595",
    "iat": 1386898951,
    "qsh": "8063ff4ca1e41df7bc90c8ab6d0f6207d491cf6dad7c66ea797b4614b71922e9",
    "exp": 1386899131
}
```

Signature:

``` html
uKqU9dTB6gKwG6jQCuXYAiMNdfNRw98Hw_IWuA5MaMo
```

### Verifying a JWT token

JWT libraries typically provide methods to be able to verify a received JWT token.
Here is an example using nimbus-jose-jwt and json-smart:

``` java
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import net.minidev.json.JSONObject;

public JWTClaimsSet read(String jwt, JWSVerifier verifier) throws ParseException, JOSEException
{
    JWSObject jwsObject = JWSObject.parse(jwt);

    if (!jwsObject.verify(verifier))
    {
        throw new IllegalArgumentException("Fraudulent JWT token: " + jwt);
    }

    JSONObject jsonPayload = jwsObject.getPayload().toJSONObject();
    return JWTClaimsSet.parse(jsonPayload);
}
```

## Advanced: Creating a JWT token manually

{{% note %}}You should only need to read this section if you are planning to create JWT tokens manually, i.e., if you are not using one of the libraries listed in the previous section.{{% /note %}}

### More details on JWT tokens

The format of a JWT token is simple: ```<base64-encoded header>.<base64-encoded claims>.<signature>```.

* Each section is separated from the others by a period character (```.```).
* Each section is base-64 encoded, so you will need to decode each one to make them human-readable.
* The header specifies a very small amount of information that the receiver needs in order to parse and verify the JWT token.
 * All JWT token headers state that the type is "JWT".
 * The algorithm used to sign the JWT token is needed so that the receiver can verify the signature.
* The claims are a list of assertions that the issuer is making: each says that "this named field" has "this value".
 * Some, like the "iss" claim, which identifies the issuer of this JWT token, have standard names and uses.
 * Others are custom claims. We limit our use of custom claims as much as possible, for ease of implementation.
* The signature is computed by using an algorithm such as HMAC SHA-256 plus the header and claims sections.
 * The receiver verifies that the signature must have been computed using the genuine JWT header and claims sections,
 the indicated algorithm and a previously established secret.
 * An attacker tampering with the header or claims will cause signature verification to fail.
 * An attacker signing with a different secret will cause signature verification to fail.
 * There are various algorithm choices legal in the JWT spec. In atlassian-connect version 1.0 we support HMAC SHA-256. **Important:** your implementation should discard any JWT tokens which specify `alg: none` as these are not subject to signature verification.


### Steps to Follow

 1. Create a header JSON object.
 * Convert the header JSON object to a UTF-8 encoded string and base-64 encode it. That gives you encodedHeader.
 * Create a claims JSON object, including a [query string hash](/cloud/bitbucket/query-string-hash)
 * Convert the claims JSON object to a UTF-8 encoded string and base-64 encode it. That gives you encodedClaims.
 * Concatenate the encoded header, a period character (`.`) and the encoded claims set. That gives you signingInput = encodedHeader + "." + encodedClaims.
 * Compute the signature of signingInput using the JWT or cryptographic library of your choice. Then base64 encode it. That gives you encodedSignature.
 * Concatenate the signing input, another period character and the signature, which gives you the JWT token. jwtToken = signingInput + "." + encodedSignature.


### Example

 Here is an example in Java using gson, commons-codec, and the Java security and crypto libraries:

``` java
public class JwtClaims {
     protected String iss;
     protected long iat;
     protected long exp;
     protected String qsh;
     protected String sub;
    // + getters/setters/constructors
}

[...]

public class JwtHeader {
    protected String alg;
    protected String typ;
     // + getters/setters/constructors
}

[...]

import static org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString;
import static org.apache.commons.codec.binary.Hex.encodeHexString;
import java.io.UnsupportedEncodingException;
import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import com.google.gson.Gson;

public class JwtBuilder {

    public static String generateJWTToken(String requestUrl, String canonicalUrl,
        String key, String sharedSecret)
                 throws NoSuchAlgorithmException, UnsupportedEncodingException,
                 InvalidKeyException {

        JwtClaims claims = new JwtClaims();
        claims.setIss(key);
        claims.setIat(System.currentTimeMillis() / 1000L);
        claims.setExp(claims.getIat() + 180L);

        claims.setQsh(getQueryStringHash(canonicalUrl));
        String jwtToken = sign(claims, sharedSecret);
        return jwtToken;
    }

    private static String sign(JwtClaims claims, String sharedSecret)
            throws InvalidKeyException, NoSuchAlgorithmException {
         String signingInput = getSigningInput(claims, sharedSecret);
         String signed256 = signHmac256(signingInput, sharedSecret);
         return signingInput + "." + signed256;
     }

     private static String getSigningInput(JwtClaims claims, String sharedSecret)
            throws InvalidKeyException, NoSuchAlgorithmException {
         JwtHeader header = new JwtHeader();
         header.alg = "HS256";
         header.typ = "JWT";
         Gson gson = new Gson();
         String headerJsonString = gson.toJson(header);
         String claimsJsonString = gson.toJson(claims);
         String signingInput = encodeBase64URLSafeString(headerJsonString
                 .getBytes())
                 + "."
                 + encodeBase64URLSafeString(claimsJsonString.getBytes());
         return signingInput;
     }

     private static String signHmac256(String signingInput, String sharedSecret)
            throws NoSuchAlgorithmException, InvalidKeyException {
        SecretKey key = new SecretKeySpec(sharedSecret.getBytes(), "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(key);
        return encodeBase64URLSafeString(mac.doFinal(signingInput.getBytes()));
    }

    private static String getQueryStringHash(String canonicalUrl)
            throws NoSuchAlgorithmException,UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
         md.update(canonicalUrl.getBytes("UTF-8"));
         byte[] digest = md.digest();
         return encodeHexString(digest);
     }
 }

[...]

public class Sample {
    public String getUrlSample() throws Exception {
        String requestUrl =
            "http://localhost:2990/jira/rest/atlassian-connect/latest/license";
        String canonicalUrl = "GET&/rest/atlassian-connect/latest/license&";
        String key = "...";     //from the app descriptor
                            //and received during installation handshake
        String sharedSecret = "..."; //received during installation Handshake

        String jwtToken = JwtBuilder.generateJWTToken(
            requestUrl, canonicalUrl, key, sharedSecret);
        String restAPIUrl = requestUrl + "?jwt=" + jwtToken;
        return restAPIUrl;
    }
}
```
