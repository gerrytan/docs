# Frameworks and tools

Atlassian Connect apps can be written using many different languages, frameworks and tools. Since Atlassian Connect 
apps operate remotely over HTTP and can be written with any programming language and web framework there are many
tools available for you to develop your apps.


## Atlassian frameworks

We've written a node.js framework to help you get started. This framework helps to generate some of the plumbing
required for your Connect app, and is officially supported by Atlassian:

 * [Atlassian Connect for Node.js Express](https://bitbucket.org/atlassian/atlassian-connect-express)
  
## Developer Tools
	
[JWT decoder](http://jwt-decoder.herokuapp.com/jwt/decode)  
An encoded JWT token can be opaque. Use this handy tool to decode JWT tokens and inspect their content. Just paste the full URL of the resource you are trying to access in the URL field, including the JWT token. For example, `https://example.atlassian.net/path/to/rest/endpoint?jwt=token`