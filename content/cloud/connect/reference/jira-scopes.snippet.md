# Scopes

Scopes allow an app to request a particular level of access to an Atlassian product.

* Within a particular product instance, an administrator may further limit app actions, allowing administrators to safely install apps they otherwise would not.
* The scopes may allow the *potential* to access beta or non-public APIs that are later changed in or removed from the
Atlassian product. The inclusion of the API endpoint in a scope does not imply that the product makes this endpoint
public. Read the [Jira Cloud platform REST API documentation](/cloud/jira/platform/rest/) for details.


## Scopes for Atlassian Connect apps

If you are building an Atlassian Connect app for Jira, use the following scopes:

| Scope name     | Description                                   |
| ---------------|---------------------------------------------- |
| `NONE` | Access app-defined data. This scope does not need to be declared in the descriptor. |
| `READ` | View, browse, and read information from Jira. |
| `WRITE` | Create or edit content in Jira, but not delete content. |
| `DELETE` | Delete content in Jira. |
| `PROJECT_ADMIN` | Administer a project in Jira. |
| `ADMIN` | Administer the Jira site. |
| `ACT_AS_USER` | Enact services on a user's behalf. |
| `ACCESS_EMAIL_ADDRESSES` | Get the email addresses of users. |


Scopes are declared as a top level attribute of `atlassian-connect.json` [app descriptor](../app-descriptor/) as in this example:

``` json
    {
        "baseUrl": "http://my-app.com",
        "key": "atlassian-connect-app",
        "scopes": [
            "read", "write"
        ],
        "modules": {}
    }
```

## OAuth 2.0 authorization code-only scopes

If your app uses [OAuth 2.0 authorization code grants (3LO)](../three-legged-oauth/) for authorization, use the following scopes:

| Scope name           | Summary     | Description                                   |
| -------------------------- | ---------------|---------------------------------------------- |
| `read:jira-user`      | View user profiles | View user information in Jira that the user has access to, including usernames, email addresses, and avatars. |
| `read:jira-work`      | View Jira issue data | Read Jira project and issue data, search for issues and objects associated with issues like attachments and worklogs. |
| `write:jira-work`      | Create and manage issues | Create and edit issues in Jira, post comments as the user, create worklogs, and delete issues. |
| `manage:jira-project`      | Manage project settings | Create and edit project settings and create new project-level objects (for example, versions and components). |
| `manage:jira-configuration`      | Manage Jira global settings | Take Jira administration actions (for example, create projects and custom fields, view workflows, and manage issue link types). |

Note that the summary of a scope is displayed to the user on the consent screen during the authorization flow.