## Personal data reporting API reference ## {#reportingapi}

The _Personal data reporting API_ is a RESTful API that allows apps to report the user accounts 
for which they are storing personal data. For flexibility and efficiency, the API allows multiple 
accounts to be reported on in a single request.

The _Personal data reporting API_ has two polling resources: one for reporting for Connect apps 
and another for reporting for OAuth 2.0 authorization code grants (3LO) apps:

- [Report accounts for Connect apps](#connectapi)
- [Report accounts for OAuth 2.0 authorization code grants (3LO) apps](#3LOapi)

These resources are described in detail below.

### Report accounts for Connect apps ### {#connectapi}

`POST /rest/atlassian-connect/latest/report-accounts`

Reports a list of user accounts and gets information on whether the personal data for each account 
needs to be updated or erased.<br>
**Authentication:** JWT authentication. See the _Authentication_ section of the 
[REST API](../rest/).

#### Parameters
This operation has no parameters.

#### Request
_Content type: application/json_<br>
Each request allows up to 90 accounts to be reported on. For each account, the 
accountId and time that the personal data was retrieved must be provided. The time format is 
defined by 
[RFC 3339, section 5.6](https://tools.ietf.org/html/rfc3339#section-5.6).<br>

Example request (application/json):

``` json
{
"accounts": [{
    "accountId": "account-id-a",
    "updatedAt": "2017-05-27T16:22:09.000Z"
  }, {
    "accountId": "account-id-b",
    "updatedAt": "2017-04-27T16:23:32.000Z"
  }, {
    "accountId": "account-id-c",
    "updatedAt": "2017-02-27T16:22:11.000Z"
  }]
}
```

#### Responses

- `200` (_Content type: application/json_): The request is successful and one or more personal data
erasure actions are required. The information is contained in an `accounts` array where each object
identifies the accountId and whether the reason for the erasure is due to the closure of
the account or invalidation of the app's copy of personal data due to the some update. In
the case of the latter, the app is permitted to re-request personal data.

Example response (application/json):

``` json
{
  "accounts": [{
    "accountId": "account-id-a",
    "status": "closed"
  }, {
    "accountId": "account-id-c",
    "status": "updated"
  }]
}
```

- `204`: The request is successful and no action by the app is required, with respect to the accounts
sent in the request.
- `400`: The request was malformed in some way. The response body contains an error message.

Example response (application/json):

``` json
{
  "errorType": "string",
  "errorMessage": "string"
}
```

- `403`: The request is forbidden.
- `429`: Rate limiting applies. The app must follow the rate limiting directives provided in response
headers.
- `500`: An internal server error occurred. The response body contains an error message.

Example response (application/json):

``` json
{
  "errorType": "string",
  "errorMessage": "string"
}
```

- `503`: The service is currently unavailable. The service may be unavailable during initial development 
or due to an outage of an upstream dependency.

### Report accounts for OAuth 2.0 authorization code grants (3LO) apps ### {#3LOapi}

`POST https://api.atlassian.com/app/report-accounts/`

Reports a list of user accounts and gets information on whether the personal data for each account 
needs to be updated or erased. This resource is for OAuth 2.0 authorization code grants (3LO) apps only.

Note, to use this API you must add the _Personal data reporting API_ to your app from the _APIs and features_ page in [app management](https://developer.atlassian.com/apps/).

**Authentication:** [OAuth 2.0 authorization code grants](../oauth-2-authorization-code-grants-3lo-for-apps/).

#### Parameters
This operation has no parameters.

#### Request
_Content type: application/json_<br>
Each request allows up to 90 accounts to be reported on. For each account, the 
accountId and time that the personal data was retrieved must be provided. The time format is 
defined by 
[RFC 3339, section 5.6](https://tools.ietf.org/html/rfc3339#section-5.6).<br>

Example request (application/json):

``` json
{
"accounts": [{
    "accountId": "account-id-a",
    "updatedAt": "2018-10-25T23:08:51.382Z"
  }, {
    "accountId": "account-id-b",
    "updatedAt": "2018-10-25T23:14:44.231Z"
  }, {
    "accountId": "account-id-c",
    "updatedAt": "2018-12-01T02:44:21.020Z"
  }]
}
```

#### Responses

- `200` (_Content type: application/json_): The request is successful and one or more personal data erasure
actions are required. The information is contained in an `accounts` array where each object
identifies the accountId and whether the reason for the erasure is due to the closure of
the account or invalidation of the app's copy of personal data due to the some update. In
the case of the latter, the app is permitted to re-request personal data.

Example response (application/json):

``` json
{
  "accounts": [{
    "accountId": "account-id-a",
    "status": "closed"
  }, {
    "accountId": "account-id-c",
    "status": "updated"
  }]
}
```

- `204`: The request is successful and no action by the app is required with respect to the accounts
sent in the request.
- `400`: The request was malformed in some way. The response body contains an error message.

Example response (application/json):

``` json
{
  "errorType": "string",
  "errorMessage": "string"
}
```

- `429`: Rate limiting applies. Delay by the time period specified in the `Retry-After` header (in seconds)
before making the API call again.
- `500`: An internal server error occurred. The response body contains an error message.

Example response (application/json):

``` json
{
  "errorType": "string",
  "errorMessage": "string"
}
```

- `503`: The service is unavailable.
