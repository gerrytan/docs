# Authentication for Connect apps

Atlassian Connect uses a technology called [JWT (JSON Web Token)](../understanding-jwt/) to authenticate apps. Basically a security context is exchanged when the app is installed, and this context is used to create and validate JWT tokens, embedded in API calls. The use of JWT tokens guarantees that:

 * The Atlassian application can verify it is talking to the app, and vice versa (authenticity).
 * None of the query parameters of the HTTP request, nor the path (excluding the context path), nor the HTTP method,
 were altered in transit (integrity).

Here is how your app can leverage Atlassian Connect's authentication feature:

1. You declare that the app uses JWT as the authentication mechanism in the app descriptor.
2. You implement an installation callback endpoint, and add a reference to it in the app descriptor.
 * When an administrator installs the app in an Atlassian cloud instance, Connect initiates an "installation handshake": it invokes the endpoint,
 passing a security context. You must then store this security context for future use.
 * The security contexts contains, among other things, a key identifying the app and a shared secret (used to create and validate JWT tokens).
3. You then use the security context to validate incoming requests (e.g. Webhooks), and sign outgoing requests (e.g. REST API calls to Jira).

### Installation handshake

The installation handshake is a way for the Atlassian application and the app to exchange keys stored on both sides for future API calls.

<img src="/cloud/connect/images/connect-installationhandshake.png" />

#### Signing of the lifecycle callbacks

When JWT authentication is used the [lifecycle](../app-descriptor/#lifecycle) callbacks are signed using a shared secret. This
shared secret is placed inside of the `Authorization` HTTP header of the lifecycle POST request.

The best way to see these JWT tokens on your lifecycle events is to use the [Connect inspector][1] to create a temporary
app, install it in your cloud development environment and watch the lifecycle events roll in. This will give you a good
understanding of the lifecycle (and Webhook) flow.

<table>
    <thead>
        <tr>
            <th>Use Case</th>
            <th>Shared Secret used to Sign</th>
        </tr>
    </thead>
    <tr>
        <td>First install</td>
        <td>None; no JWT token. Because there was no previous shared secret the recipient cannot validate a JWT token.
        This means that you should anticipate that there will be no <code>Authorization</code> header present.</td>
    </tr>
    <tr>
        <td>Second and subsequent installs</td>
        <td>The shared secret sent in the preceding <code>installed</code> callback.</td>
    </tr>
    <tr>
        <td>Uninstall, Enable & Disable</td>
        <td>The shared secret sent in the preceding <code>installed</code> callback.</td>
    </tr>
    <tr>
        <td>First install after being uninstalled</td>
        <td>The shared secret sent in the preceding <code>installed</code> callback. This allows apps to allow the new installation to access previous tenant data (if any exists).<br>
            A valid signature demonstrates that the sender is in possession of the shared secret from when the old tenant data was accessed.</td>
    </tr>
</table>

### Making a service call

#### Using JWT

To call an API exposed by an Atlassian product, the app must add a valid JWT token to the request.
<img src="/cloud/connect/images/connect-servicecall-jwt.png" />

#### Using user impersonation

To call an API exposed by an Atlassian product and initiate a service on behalf of a user, the app must add a valid OAuth 2.0 `access token` to the request.
To get an OAuth 2.0 `access token`, the app exchanges a signed JWT for an OAuth 2.0 `access token` through the Atlassian authorization server.
<img src="/cloud/connect/images/connect-servicecall-oauth2bearer.png" />

### Exposing a service

When an Atlassian product calls an API exposed by the app, it is the app's responsibility to validate
the JWT token, using the security context provided during the installation handshake.
<img src="/cloud/connect/images/connect-exposeservice.png" />

## Authentication how-to

### Creating the app descriptor

For an Atlassian Connect app to authenticate securely with the host Atlassian product, it must advertise itself as
being JWT aware, and provide a resource to receive important installation information. This is done by specifying the
elements `authentication` and `lifecycle`.

The `lifecycle:installed` property is a url which is synchronously called by the Atlassian application when the app
is installed.

For example:

``` json

    {
        "baseUrl": "http://localhost:3000",
        "key": "atlassian-connect-app",
        "authentication": {
            "type": "jwt"
        },
        "lifecycle": {
            "installed": "/app-installed-callback"
        }
        "modules": {} // etc
    }
```

<div class="aui-message note">
    <div class="icon"></div>
    <p>
    Upon successful registration, the app must return either a <code>200 OK</code> or <code>204 No Content</code> response code, otherwise
    the operation will fail and the installation will be marked as incomplete.
    </p>
</div>


<a name='installation'></a>
### Installation data

When the app is installed, the Atlassian application invokes a callback endpoint exposed by the app.
The request contains a payload with important tenant information that you will need to store in your app in
order to sign and verify future requests.

For details on the contents of the payload, please see the [lifecycle attribute](../app-descriptor/) documentation.

### Making a service Call

The JWT protocol describes the format and verification of individual JWT tokens. However it does not prescribe a method
of transportation. Connect transports JWT tokens as query-string parameters and as authorization headers.
When communicating server-to-server with the Atlassian host product your app must include a JWT token when accessing
protected resources. This covers most of the REST APIs. Construct a token that identifies your app, identifies the query,
specifies the token's expiry time and allows the receiver to verify that this token was genuinely constructed by your app.
You must use one of the following methods to add the JWT token to the API call:

Query string example:

``` javascript   
GET https://<my-dev-environment>.atlassian.net/jira/rest/api/2/issue/AC-1.json?jwt=<jwt-token>
```

Headers example:

``` javascript
POST https://<my-dev-environment>.atlassian.net/jira/rest/api/2/issue/AC-1/attachments
"Authorization" header value: "JWT <jwt-token>"
```

For more details on how to create a JWT token, see [Creating a JWT Token](../understanding-jwt/#a-name-creating-token-a-creating-a-jwt-token).

<a name='exposing'></a>
### Exposing a service

All incoming requests (requests coming from an Atlassian product) should check for the presence of the `jwt`
query string parameter, which needs to be decoded and verified. In particular, the verification should:

1. Extract the JWT token from the request's `jwt` query parameter or the authorization header.
* Decode the JWT token, without verification. This gives you a header JSON object, a claims JSON object, and a signature.
* Extract the issuer (`iss`) claim from the decoded, unverified claims object. This is the `clientKey` for the tenant -
an identifier for the Atlassian application making the call, which should have been stored by the app as part of the
[installation handshake](#installation).
* Look up the `sharedSecret` for the `clientKey`, as stored by the app during the installation handshake
* Verify the signature with the `sharedSecret` and the algorithm specified in the header's `alg` field.
* Verify the query has not been tampered by [Creating a Query Hash](../understanding-jwt/#a-name-qsh-a-creating-a-query-string-hash) and comparing it against the
`qsh` claim on the verified token.
* The JWT specification lists some [standard claims](http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-13#section-4.1.1)
that, if present, you should verify. Issuers include these to help you ensure that tokens you receive are used according to the intentions of
the issuer and with the best possible results.

These steps must be executed before processing the request, and the request must be rejected if any of these steps fail.

For more details on how to decode and validate a JWT token, see [Decoding and Validating a JWT Token](../understanding-jwt/#a-name-decoding-token-a-decoding-a-jwt-token),
which also provides a comprehensive list of claims supported by Atlassian products that you need to validate.

 [1]: https://connect-inspector.prod.public.atl-paas.net/
