---
title: "My requests in Jira Service Desk"
platform: cloud
product: jsdcloud
category: devguide
subcategory: learning
guides: tutorials
date: "2017-10-25"
---

# My requests in Jira Service Desk

This tutorial shows you how to build a static Atlassian Connect app that displays the list of Jira
Service Desk requests made by the currently logged-in user on a page accessible from the agent portal.

As part of the tutorial, you'll learn about:

* [Configuring your development environment](#setting-up)
* [Deploying a Connect app to a Jira Service Desk](#deploying)
* [Extending the Jira Service Desk UI](#extending-ui)
* [Using the Jira Service Desk REST API](#using-api)

Using [Node.js](https://nodejs.org) and the [Atlassian Connect Express (ACE)](https://bitbucket.org/atlassian/atlassian-connect-express)
toolkit, you will build an app that will interface with Jira Service Desk via the new Service Desk REST API, and will
make use of the [AUI](https://docs.atlassian.com/aui/latest) library for front-end styling.

The finished result will look similar to this:

<img src="/cloud/connect/images/my-requests-3.png" width="80%" style="border:1px solid #999;margin-top:10px;" />

{{% note %}}
The code for this tutorial is available in the [atlassianlabs/jira-servicedesk-my-requests-tutorial](https://bitbucket.org/atlassianlabs/jira-servicedesk-my-requests-tutorial) repo.
{{% /note %}}

## Setting up your development environment

In this section, you'll install Node.js, and the Atlassian Connect Express (ACE) toolkit. Next,
you'll use ACE to generate a new Atlassian Connect app project that includes a small "Hello, world!" example page.
Finally, you'll deploy the generated app to a running cloud development instance of Jira Service Desk.

### Installing the toolchain

1. Install [Node.js](https://nodejs.org) 4.5.0 or later:
  - If you use [Homebrew](http://brew.sh), use the following command (you might need to enter sudo):

    ``` bash
brew install node
    ```
  - If you use Windows or don't use Homebrew, just [download and install Node.js](https://nodejs.org/en/download/) directly.
1. Install ACE using the following npm installation command:

    ``` bash
npm install -g atlas-connect
    ```

The [ACE](https://bitbucket.org/atlassian/atlassian-connect-express) toolkit helps you create Connect apps using Node.js. ACE handles JSON web tokens (JWT), so that requests between your app and the Jira application are signed and authenticated.

### Creating the app project

1. Use ACE to create a new Node.js project called `sd-my-requests`:

   ``` bash
atlas-connect new -t jira sd-my-requests
   ```

1. Change to your new `sd-my-requests` directory:

   ``` bash
cd sd-my-requests
   ```

1. Install the Node.js dependencies for your `sd-my-requests` project:

   ``` bash
npm install
   ```

The generated project contains a simple example "Hello, world!" page, which when deployed will be accessible from a
button on Jira's main navigation bar.

Feel free to open the `atlassian-connect.json` descriptor file and edit the name, description, and vendor properties.
Don't change anything else at this stage.

### Get a development version of Jira Service Desk

Follow the [Getting started](../getting-started/#set-up-a-development-environment) guide to get a development version of Jira and Jira Service Desk.

Since we are developing with a local app against a cloud product, we will need to setup our environment to make our app accessible.

You'll also need to create a test Service Desk project along with some sample requests:

1. Create a new Service Desk project via the "Projects" dropdown on the main navigation bar
1. From within your new Service Desk project, click the "Customer channels" item on the sidebar, then "Visit the portal"
1. Finally, from within the Customer Portal, create a few sample requests that we'll later use to test our finished
   product!

## Getting things running

It's time to see your app in action! In this section, you'll deploy your app to your cloud instance of Jira
Service Desk. You'll also extend the Jira Service Desk UI by making use of an extension point inside the agent portal, and
call the Jira Service Desk REST API to retrieve your list of requests from the instance.

### Deploying the app

Deploy your app by using this simple command from the `sd-my-requests` directory:

``` bash
npm start
```
Since you are using ACE, this will start a local HTTP server to host your app.

To install the app, you will need to go to the __Manage apps__ page in Jira, click the __Upload app__ and
[paste the ngrok link to your app](../getting-started/#step-3-set-up-your-local-development-environment).

Your app has now been deployed! Check it out by visiting your Jira Service Desk.
You will see a "Hello World" button in the main Jira navigation bar, which will take you to the "Hello,
world!" example page!

<img src="/cloud/connect/images/my-requests-1.png" width="80%" style="border:1px solid #999;margin-top:10px;" />

### Extending the Jira Service Desk UI
Atlassian Connect apps can be used to extend the UI of Atlassian products, via various extension points that are built
into various pages across the applications. The `atlassian-connect.json` descriptor is used to define Web UI modules,
which can be attached to extension points to inject additional content into pages (or add entirely new pages).
Documentation on extension point locations is available for Jira [here](https://developer.atlassian.com/display/JiraDEV/Web+Fragments).
Additionally, see *Common Modules* and *Jira Modules* for more information on Web UI modules.

Let's try it out. In this section, you'll extend the Jira Service Desk UI by adding a new page that displays all the
requests made by the currently logged-in user:

1. Go to the `/views` subdirectory, in your `sd-my-requests` directory.
1. Create a file named `my-requests.hbs` and add the following content using an editor:

   ``` html
{{!< layout}}

<section class="aui-page-panel-item">
    <div class="aui-group">
        <div id="main-content" class="aui-item"></div>
    </div>
</section>
   ```
   This file will serve as the template for your "My Requests" page.
1. Go to the `/routes` subdirectory, in your `sd-my-requests` directory.
1. Edit the `index.js` file and add the following route handling code, after the `/hello-world` route:

   ``` javascript
app.get('/my-requests', addon.authenticate(), function (req, res) {
   res.render('my-requests');
   });
   ```
1. Check that the file now looks like the following:

   ``` javascript
module.exports = function (app, addon) {

   // Root route. This route will serve the `atlassian-connect.json` unless the
   // documentation url inside `atlassian-connect.json` is set
   app.get('/', function (req, res) {
       res.format({
           // If the request content-type is text-html, it will decide which to serve up
           'text/html': function () {
               res.redirect('/atlassian-connect.json');
           },
           // This logic is here to make sure that the `atlassian-connect.json` is always
           // served up when requested by the host
           'application/json': function () {
               res.redirect('/atlassian-connect.json');
           }
       });
   });

   // This is an example route that's used by the default "generalPage" module.
   // Verify that the incoming request is authenticated with Atlassian Connect
   app.get('/hello-world', addon.authenticate(), function (req, res) {
           // Rendering a template is easy; the `render()` method takes two params: name of template
           // and a json object to pass the context in
           res.render('hello-world', {
               title: 'Atlassian Connect'
               //issueId: req.query['issueId']
           });
       }
   );

   // Add any additional route handlers you need for views or REST resources here...
   // My Requests page
   app.get('/my-requests', addon.authenticate(), function (req, res) {
       res.render('my-requests');
   });

   // load any additional files you have in routes and apply those to the app
   {
       var fs = require('fs');
       var path = require('path');
       var files = fs.readdirSync("routes");
       for(var index in files) {
           var file = files[index];
           if (file === "index.js") continue;
           // skip non-javascript files
           if (path.extname(file) != ".js") continue;

           var routes = require("./" + path.basename(file));

           if (typeof routes === "function") {
               routes(app, addon);
           }
       }
   }
};
   ```
   You now have a page!
1. Open your `atlassian-connect.json` descriptor and remove everything inside the `modules` object.
1. Add a `jiraProjectTabPanels` section inside the `modules` object:

   ``` json
"jiraProjectTabPanels": [
   {
       "key": "my-requests-link",
       "name": {
           "value": "My Requests"
       },
       "url": "/my-requests",
       "conditions": [
           {
               "condition": "user_is_logged_in"
           },
           {
               "condition": "can_use_application",
               "params": {
                   "applicationKey": "jira-servicedesk"
               }
           }
       ]
   }
]
   ```
1. Verify that the file now looks like the following:

  ``` json
{
   "key": "sd-my-requests",
   "name": "My Requests",
   "description": "My very first app",
   "vendor": {
       "name": "Angry Nerds",
       "url": "https://www.atlassian.com/angrynerds"
   },
   "baseUrl": "{{localBaseUrl}}",
   "links": {
       "self": "{{localBaseUrl}}/atlassian-connect.json",
       "homepage": "{{localBaseUrl}}/atlassian-connect.json"
   },
   "authentication": {
       "type": "jwt"
   },
   "lifecycle": {
       // atlassian-connect-express expects this route to be configured to manage the installation handshake
       "installed": "/installed"
   },
   "scopes": [
       "READ"
   ],
   "modules": {
       "jiraProjectTabPanels": [
           {
               "key": "my-requests-link",
               "name": {
                   "value": "My Requests"
               },
               "url": "/my-requests",
               "conditions": [
                   {
                       "condition": "user_is_logged_in"
                   },
                   {
                       "condition": "can_use_application",
                       "params": {
                           "applicationKey": "jira-servicedesk"
                       }
                   }
               ]
           }
       ]
   }
}
   ```
1. Restart your Atlassian Connect app.
1. Reinstall the Connect app on your Jira Service Desk.

You have now extended the Jira Service Desk UI with your app! Check it out by going to the agent portal of your
previously created Service Desk project. You will see a "My Requests" button under the "Apps" tab in the Jira sidebar, which will take you to
the "My Requests" page!

<img src="/cloud/connect/images/my-requests-2.png" width="80%" style="border:1px solid #999;margin-top:10px;" />

### Using the Jira Service Desk REST API
The Jira Service Desk REST API is how Atlassian Connect apps communicate with Jira Service Desk, either for
retrieving data or sending it. For more information, see the [Jira Service Desk REST API reference (Cloud)](/cloud/jira/service-desk/rest/).

For the purposes of this tutorial, you'll only be using a single REST endpoint: `/rest/servicedeskapi/request`. This
endpoint is used to retrieve the list of requests made by the currently logged-in user.

The steps below will show you how to call the Jira Service Desk REST API to retrieve the list of requests, then create
and populate a table with the results:

1. Go to the `/public/js` subdirectory, in your `sd-my-requests` directory.
1. Edit the `addon.js` file, and add the following code:

   ``` javascript
$(document).ready(function () {
   // Require the request module
   AP.require(['request', 'messages'], function(request, messages) {
       // GET our Service Desk requests via the Jira Service Desk REST API
       request({
           url: '/rest/servicedeskapi/request',
           success: function (response) {
               // Parse the response JSON
               var json = JSON.parse(response);

               // Store the base URL for later
               var baseUrl = json._links.base;

               // Did we get any requests back?
               if (json.values.length > 0) {
                   // Create a table with the resulting requests
                   $('<table>').addClass('aui').append(
                       $('<thead>').append(
                           $('<tr>').append(
                               $('<th>').text('Issue Key'),
                               $('<th>').text('Current Status'),
                               $('<th>').text('Summary'),
                               $('<th>').text('Date Created'),
                               $('<th>')
                           )
                       ),
                       $('<tbody>').append(
                           $.map(json.values, function (e) {
                               // Map each request to a HTML table row
                               return $('<tr>').append(
                                   $('<td>').append(
                                       $('<a>').attr('href',
                                                     baseUrl + '/browse/' + e.issueKey)
                                               .attr('target', '_top')
                                               .text(e.issueKey)
                                   ),
                                   $('<td>').text(e.currentStatus.status),
                                   $('<td>').text(e.requestFieldValues[0].value),
                                   $('<td>').text(e.createdDate.friendly),
                                   $('<td>').append(
                                       $('<a>').attr('href',
                                                     baseUrl + '/servicedesk/customer/portal/' + e.serviceDeskId + '/' + e.issueKey)
                                               .attr('target', '_blank')
                                               .text('View in customer portal')
                                   )
                               );
                           })
                       )
                   ).appendTo('#main-content');
               } else {
                   // Show a link to the Customer Portal
                   $('<div>').addClass('aui-message').append(
                       $('<p>').addClass('title').append(
                           $('<span>').addClass('aui-icon').addClass('icon-info'),
                           $('<strong>').text("It looks like you don't have any requests!")
                       ),
                       $('<p>').append(
                           $('<span>').text("Visit the "),
                           $('<a>').attr('href',
                                         baseUrl + '/servicedesk/customer/portals')
                                   .attr('target', '_blank')
                                   .text('Customer Portal'),
                           $('<span>').text(" to create some.")
                       )
                   ).appendTo('#main-content');
               }
           },
           error: function (err) {
               $('<div>').addClass('aui-message').addClass('aui-message-error').append(
                   $('<p>').addClass('title').append(
                       $('<span>').addClass('aui-icon').addClass('icon-error'),
                       $('<strong>').text('An error occurred!')
                   ),
                   $('<p>').text(err.status + ' ' + err.statusText)
               ).appendTo('#main-content');
           }
       });
   });
});
   ```
1. Restart your Atlassian Connect app.

Your app can now make requests to the Jira Service Desk REST API! Check it by going to your "My Requests" page. You
will see a table populated with all your requests!

## The finished result

![My requests](/cloud/connect/images/my-requests-3.png)

Congratulations! You've successfully built your first Atlassian Connect app for Jira Service Desk!

You should now have a basic understanding of the following:

* how to create an Atlassian Connect app in Node.js with ACE
* how to extend the Jira Service Desk UI via extension points specified in the `atlassian-connect.json` descriptor file
* how to retrieve data via the Jira Service Desk REST API

## Additional resources

- [Jira extension points](https://developer.atlassian.com/display/JiraDEV/Web+Fragments)
- [atlassian-connect.json descriptor](../about-jira-modules/)
- [Jira Service Desk Cloud REST API](/cloud/jira/service-desk/rest/)