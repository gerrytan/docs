---
title: Scopes
platform: cloud
product: jsdcloud
category: devguide
subcategory: blocks
aliases:
- /cloud/jira/service-desk/scopes.html
- /cloud/jira/service-desk/scopes.md
date: "2018-12-17"
---
{{< include path="docs/content/cloud/connect/reference/jira-scopes.snippet.md">}}

### OAuth 2.0 authorization code-only scopes for Jira Service Desk

The following OAuth 2.0 authorization code grants (3LO) scopes only apply to Jira Service Desk:

| Scope name           | Summary     | Description                                   |
| -------------------------- | ---------------|---------------------------------------------- |
| `read:servicedesk-request`      | View Jira Service Desk request data | Read customer request data, including approvals, attachments, comments, request participants, and status/transitions.<br>Read service desk and request types, including searching for request types and reading request type fields, properties and groups. |
| `write:servicedesk-request`      | Create and manage Jira Service Desk requests | Create and edit customer requests, including add comments and attachments, approve, share (add request participants), subscribe, and transition. |
| `manage:servicedesk-customer`      | Manage Jira Service Desk customers and organizations | Create, manage and delete customers and organizations.<br>Add and remove customers and organizations from service desks. |