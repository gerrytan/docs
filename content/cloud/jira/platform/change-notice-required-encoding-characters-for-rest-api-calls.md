---
title: "Change notice - Required encoding of some characters used in REST API calls"
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
date: "2018-10-08"
---

# Change notice - Required encoding of some characters used in REST API calls

As part of our continued focus on the security of our cloud platform, and a
recent Tomcat update we've made, we're introducing changes that'll require the
encoding of some characters used in REST API calls. We'll make these changes on
 **1 April 2019**, giving developers and consumers of Jira
Cloud REST APIs six months to make any necessary changes. These changes are *not*
related to other GDPR-related API updates we're currently making.

## Unsupported URL characters

These changes require the following characters to be encoded:

``` text
" < > [ \ ] ^ ` { | }
```

Using these characters in a browser will auto-encode them, but URLs including
the characters will not be supported when left unencoded and used
programmatically in scripts or API calls.

## Developer Workaround

To avoid any calls to Jira APIs breaking because of this change, you'll need to
ensure URL characters are encoded. You can do this using standard URL encoding
functions in all programming languages.

## Time frame for changes

As mentioned above, these changes take effect on April 1st, 2019.