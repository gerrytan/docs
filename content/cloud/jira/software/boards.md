---
title: "Boards" 
platform: cloud
product: jswcloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-software-modules-boards-39990330.html
- /jiracloud/jira-software-modules-boards-39990330.md
confluence_id: 39990330
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39990330
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39990330
date: "2017-09-11"
---
# Boards

Apps can add content to the main board view in Jira Software, and to the board 
settings area if you have configuration options that are available on a per-board 
basis.

## Board area

You can add a dropdown menu or button to a board, next to the **Boards** menu.

### Available locations

You can define a `web section` that will be rendered as a dropdown in the top right 
corner of the screen using the `jira.agile.board.tools`. 

You can then create `web items` that use this web section as a location to populate 
the dropdown. 

![Board dropdown location](../images/board-dropdown-location.png)

#### Sample Descriptor JSON
``` json
...
"modules": {
    "webSections": [
        {
            "key": "board-links",
            "location": "jira.agile.board.tools",
            "weight": 10,
            "name": {
                "value": "My board app"
            }
        }
    ],   
    "webPanels": [
        {
            "key": "my-web-panel",
            "url": "web-panel?id={board.id}&mode={board.screen}",
            "location": "board-links",
            "name": {
                "value": "My Web Panel"
            },
            "layout": {
                "width": "100px",
                "height": "100px"
            }
        }
    ]
}
...
```

### Properties

The properties required for this location are the standard ones defined in the 
documentation for [web sections] and [web items].

----

## Board configuration

You can also provide a web panel in the board configuration section of the board (
accessible by clicking the (•••) menu on the board then *Board settings*).

### Available locations

For web panels:
* `jira.agile.board.configuration`

You don't need to declare a separate `web item`. Once you declare your `web panel` in 
this location, the link in the board settings sidebar is created automatically. See 
the sample descriptor JSON below.

![Board configuration location](../images/board-configuration-location.png)

#### Sample descriptor JSON
``` json
...
"modules": {       
    "webPanels": [
        {
            "key": "my-configuration-page",
            "url": "configuration?id={board.id}&type={board.type}",
            "location": "jira.agile.board.configuration",
            "name": {
                "value": "My board configuration page"
            },
            "weight": 10
        }
    ]
}
...
```

### Properties

The properties required for this location are the standard ones defined in the documentation for [web panels].


[i18n property]: /cloud/jira/software/modules/i18n-property
[additional context]: /cloud/jira/software/context-parameters
[web sections]: /cloud/jira/software/modules/web-section
[web items]: /cloud/jira/software/modules/web-item/
[web panels]: /cloud/jira/software/modules/web-panel/