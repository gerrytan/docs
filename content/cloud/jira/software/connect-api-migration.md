---
title: "Connect API migration"
platform: cloud
product: jswcloud
category: devguide
subcategory: learning
date: "2018-09-01"
aliases:
- /cloud/jira/software/connect-api-migration.html
- /cloud/jira/software/connect-api-migration.md
---
{{< include path="docs/content/cloud/connect/concepts/connect-api-migration.snippet.md">}}
