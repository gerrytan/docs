---
title: "Authentication for apps"
platform: cloud
product: jswcloud
category: devguide
subcategory: security
date: "2017-08-24"
---
{{< include path="docs/content/cloud/connect/concepts/authentication.snippet.md" >}}