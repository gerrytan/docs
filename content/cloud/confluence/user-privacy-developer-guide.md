---
title: "User Privacy Developer Guide"
platform: cloud
product: confcloud
category: devguide
subcategory: other
date: "2019-04-09"
---

{{< include path="docs/content/cloud/connect/guides/user-privacy-developer-guide.snippet.md" >}}
{{< include path="docs/content/cloud/connect/guides/personal-data-reporting-api.snippet.md" >}}