---
title: "OAuth 2.0 bearer tokens for apps"
platform: cloud
product: confcloud
category: devguide
subcategory: security
date: "2019-04-02"
---
{{< include path="docs/content/cloud/connect/concepts/oauth2-jwt-bearer-token-authentication.snippet.md">}}
