---
title: "Authentication for apps"
platform: cloud
product: confcloud
category: devguide
subcategory: security
date: "2017-08-28"
---
{{< include path="docs/content/cloud/connect/concepts/authentication.snippet.md" >}}