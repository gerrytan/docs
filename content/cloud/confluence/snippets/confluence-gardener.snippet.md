---
title: "Confluence gardener"
platform: cloud
product: confcloud
category: devguide
subcategory: learning
guides: tutorials
date: "2017-10-24"
---

<!-- AUTHOR'S NOTE
This is being stored as a snippet for now because the tutorial does not work (snippets are not
published as standalone pages). It was last updated in 2014. Once this tutorial is updated, you
simply need to remove the .snippet from the file name and move the file into the main
content/cloud/confluence directory. It should then populate on the Tutorials and guides page.
-->

# Confluence gardener tutorial

This tutorial shows you how to build a static Connect app that displays page hierarchy in a
Confluence space. This app is handy to query, update, and delete Confluence pages. You'll also
create a full-screen confirmation dialog displaying content from your app.

Your app will use the [Confluence REST API](/cloud/confluence/rest/). At completion, your
app will look a lot like this:

![image](/cloud/connect/images/confluence-gardener-screen.png)

## Before you begin

To complete this tutorial, you’ll need a basic understanding of Confluence and the following:

- [Git](http://git-scm.com/)
- [Node.js](http://nodejs.org/)
- Cloud development site with development mode enabled, see [Cloud development site setup](../getting-started#cloud-development-site)

1. Clone the [Confluence Gardener](https://bitbucket.org/atlassianlabs/confluence-gardener.git) repository.

    ``` bash
    git clone https://bitbucket.org/atlassianlabs/confluence-gardener.git
    ```
1. Change into your new `confluence-gardener` directory.

    ``` bash
    cd confluence-gardener
    ```

## Host and install your app

Confluence Gardener is a static Connect app that can be served by a simple web server and cached
with a CDN. We'll use a simple Node.js-powered static web server to host your app locally.

After you've spun up your server, you'll install your copy of Gardener to Confluence. You'll use a
bash script  included in the repo you cloned.

1. From the `confluence-gardener` directory, start your server on port 8000:

    ``` bash
    npm install -g http-server
    http-server -p 8000
    ```
The server indicates that it is serving HTTP at the current address and port. You'll see something like this:

    ``` bash
    Starting up http-server, serving ./ on: http://0.0.0.0:8000
    ```
1. In your browser, navigate to your descriptor file at [http://localhost:8000/atlassian-connect.json](http://localhost:8000/atlassian-connect.json).

    ``` javascript
        {
            "key": "confluence-gardener",
            "name": "Confluence Gardener",
            "description": "Prune back your Confluence page graph.",
            "baseUrl": "https://addon-dev-url.ngrok.io",
            "vendor": {
                "name": "Atlassian Labs",
                "url": "https://www.atlassian.com"
            },
            "authentication": {
                "type": "none"
            },
            "version": "0.1",
            "modules": {
                "generalPages": [
                     {
                         "key": "gardener",
                         "url": "/index.html?spaceKey={space.key}",
                         "location": "system.content.action",
                         "name": {
                             "value": "Confluence Gardener"
                         }
                     }
                 ]
            },
            "scopes": [
                "read",
                "write",
                "delete"
            ]
        }
    ```
1. For the next step you will need to host your app so that it is accessible anywhere on the internet.
1. Install Gardener using the Universal Plugin Manger:
   1.  Navigate to *https://&lt;my-dev-environment&gt;.atlassian.net/wiki/plugins/servlet/UPM*
   1. Click **Upload app**.
   1. Enter a URL that points to your descriptor. For example *https://&lt;my-addon-url&gt;.ngrok.io/atlassian-connect.json*
   1. Click **Upload**.
    Gardener doesn't have functionality yet (you'll implement that in future steps), but feel free to try to load it anyway.
1. Create a space in your development version of Confluence and navigate to the space.
1. Click **Tools** at the top right, and choose **Confluence Gardener**. You should see a page like this:
    ![confluence gardener](/cloud/connect/images/confluence-gardener-0.png)

Now you're ready to start developing functionality for your Gardener app.

## <a name="rest-api"></a> Implement a Confluence REST API client

All the functions that request data from Confluence are in your `js/data.js` file. The functions are incomplete,
so in this step you'll flesh these out.

You'll use the [Confluence REST API Docs](/cloud/confluence/rest/) and
the [AP.request documentation](../javascript/module-request.html) to implement functions to get
page and space hierarchy in Confluence, and add Gardener functionality to move and remove pages.

1. Open the `js/data.js` file from your `confluence-gardener` source code.
    You should see the stub code below:

    ``` javascript
    define(function() {
        return {
            getPageContentHierarchy: function(pageId, callback) {
            },

            getSpaceHierarchy: function(spaceKey, callback) {
            },

            removePage: function(pageId, callback) {
            },

            movePage: function(pageId, newParentId, callback) {
            },

            movePageToTopLevel: function(pageId, spaceKey, callback) {
            }
        }
    });
    ```
1. Implement `getPageContentHierarchy` to get the page hierachy in your Confluence instance:

    ``` javascript
    getPageContentHierarchy: function(pageId, callback) {
        AP.request({
            url: "/rest/prototype/1/content/" + pageId + ".json?expand=children",
            success: callback
        });
    },
    ```
1.  Next, implement `getSpaceHierarchy` to see the space hierarchy:

    ``` javascript
    getSpaceHierarchy: function(spaceKey, callback) {
        AP.request({
            url: "/rest/prototype/1/space/" + spaceKey + ".json?expand=rootpages",
            success: callback
        });
    },
    ```
1. Implement `removePage` so your app can effectively delete Confluence pages:

    ``` javascript
    removePage: function(pageId, callback) {
        AP.request({
            url: "/rpc/json-rpc/confluenceservice-v2/removePage",
            contentType: "application/json",
            type: "POST",
            data: JSON.stringify([pageId]),
            success: callback
        });
    },
    ```
1. Finally, try to implement `movePage` and `movePageToTopLevel` on your own.
    If you get stuck, take a look at the example below.

    ``` javascript
    define(function() {
        return {
            getPageContentHierarchy: function(pageId, callback) {
                AP.request({
                    url: "/rest/prototype/1/content/" + pageId + ".json?expand=children",
                    success: callback
                });
            },

            getSpaceHierarchy: function(spaceKey, callback) {
                AP.request({
                    url: "/rest/prototype/1/space/" + spaceKey + ".json?expand=rootpages",
                    success: callback
                });
            },

            removePage: function(pageId, callback) {
                AP.request({
                    url: "/rpc/json-rpc/confluenceservice-v2/removePage",
                    contentType: "application/json",
                    type: "POST",
                    data: JSON.stringify([pageId]),
                    success: callback
                });
            },

            movePage: function (pageId, newParentId, callback) {
                AP.request({
                    url: "/rpc/json-rpc/confluenceservice-v2/movePage",
                    contentType: "application/json",
                    type: "POST",
                    data: JSON.stringify([pageId, newParentId, "append"]),
                    success: callback
                });
            },

            movePageToTopLevel: function(pageId, spaceKey, callback) {
                AP.request({
                    url: "/rpc/json-rpc/confluenceservice-v2/movePageToTopLevel",
                    contentType: "application/json",
                    type: "POST",
                    data: JSON.stringify([pageId, spaceKey]),
                    success: callback
                });
            }
        }
    });
    ```
1. Now, refresh Gardener in your browser.
    You should see a page like this:
    ![confluence gardener](/cloud/connect/images/confluence-gardener-.5.png)
    Dark blue names indicate the space names, and light blue signifies pages with children.
1. Click a light blue name, like the __Welcome to the Confluence Demonstration Space__ page name.
    You should see the menu snap open to display the page children:
    ![confluence gardener](/cloud/connect/images/confluence-gardener-1.png)
    Blue pages have children, whereas gray pages have no child pages underneath. You can also use your mouse to zoom in and out -
    just scroll your trackball up and down.

As you explore your Gardener app, you might notice that you're not able to actually remove pages. Let's fix that in the next
step.

## <a name="dialog"></a> Display a full-screen dialog

When you attempt to remove a page, nothing happens. In this step, you'll add a [a full-screen
dialog](../javascript/module-Dialog.html) to confirm the action,
and make sure it actually works.

First, you'll declare the dialog in your `atlassian-connect.json` descriptor file. These dialogs are full-fledged AUI dialogs
that exist in the parent frame (not in the same iframe Gardener uses). We'll provide the HTML source for the dialog, you'll
register a new [`webItem`](/cloud/jira/platform/modules/web-item.html) that loads inside the
full-screen dialog.

1. Open `atlassian-connect.json` in your editor.
1. Add the following snippet after the `generalPages` entry in the `modules` object:

    ``` json
    "webItems": [
        {
            "key": "gardener-remove-dialog",
            "url": "/remove-page-dialog.html",
            "location": "system.top.navigation.bar",
            "weight": 200,
            "context": "addon",
            "target": {
                "type": "dialog",
                "options": {
                    "width": "234px",
                    "height": "324px"
                }
            },
            "name": {
                "value": "dialog"
            }
        }
    ]
    ```
Atlassian-connect.json [+]</a> with the dialog `webItems` entry

    ``` json
    {
        "key": "confluence-gardener",
        "name": "Confluence Gardener",
        "description": "Prune back your Confluence page graph.",
        "baseUrl": "https://add-on-dev-url.ngrok.io",
        "vendor": {
            "name": "Atlassian Labs",
            "url": "https://www.atlassian.com"
        },
        "authentication": {
            "type": "none"
        },
        "version": "0.1",
        "modules": {
        "generalPages": [
             {
                 "key": "gardener",
                 "url": "/index.html?spaceKey={space.key}",
                 "location": "system.content.action",
                 "name": {
                     "value": "Confluence Gardener"
                 }
             }
         ],
         "webItems": [
             {
                 "key": "gardener-remove-dialog",
                 "url": "/remove-page-dialog.html",
                 "location": "system.top.navigation.bar",
                 "weight": 200,
                 "context": "addon",
                 "target": {
                     "type": "dialog",
                     "options": {
                         "width": "234px",
                         "height": "324px"
                     }
                 },
                 "name": {
                     "value": "dialog"
                 }
             }
         ]
        },
        "scopes": [
            "read",
            "write",
            "delete"
        ]
    }
    ```
1. Reinstall your app through the UPM **Manage apps** page.
    Your app won't show any changes - yet. In the next step, you'll implement a function to display the dialog.
1. In your editor, open `removePageDialog.js`.
    You should see another empty function:

    ``` javascript
    define(function() {
        return function (deleteCallback) {
        }
    });
    ```
1. Load the [`dialog`](../javascript/module-Dialog.html)
    and [`events`](../javascript/module-Events.html) modules using `AP.require`:

    ``` javascript
    return function(deleteCallback) {
        AP.require(["dialog", "events"], function (dialog, events) {
            dialog.create({
                key: 'gardener-remove-dialog',
                width: "400px",
                height: "80px",
                header: "Remove page?",
                submitText: "Remove",
                cancelText: "cancel",
                chrome: true
            });
    ```
1. After creating the dialog, unsubscribe from any previous `confirmPageRemoval` event bindings:
   {{< highlight javascript >}}events.offAll("confirmPageRemoval");{{< /highlight >}}
1. Now, try subscribing to `confirmPageRemoval` event bindings using `events.on` to register the event and
   passing `deleteCallback`. removePageDialog.js with subscription to `confirmPageRemoval`

``` javascript
    define(function() {
        return function(deleteCallback) {
            AP.require(["dialog", "events"], function (dialog, events) {
                dialog.create({
                    key: 'gardener-remove-dialog',
                    width: "400px",
                    height: "80px",
                    header: "Remove page?",
                    submitText: "Remove",
                    cancelText: "cancel",
                    chrome: true
                });

                events.offAll("confirmPageRemoval");
                events.on("confirmPageRemoval", deleteCallback);
            });
        }
    });
```
1. In Confluence Gardener, click __Remove__ on a page.
    You should see a dialog appear:
    ![confluence gardener dialog](/cloud/connect/images/confluence-gardener-dialog.png)
    If you don't see a dialog, try a hard refresh, or turning off the cache in your browser developer console.

## What's next?

You've built an app that can help you prune pages from Confluence spaces. Now you're ready to explore our documentation.
How about reading through:

 * The [JavaScript API][3].
 * The [Frameworks and Tools][4] available to you.

  [3]: ../concepts/javascript-api.html
  [4]: ../developing/frameworks-and-tools.html
